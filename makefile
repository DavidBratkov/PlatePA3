plate: PlatePA3.o hash.o list.o Hash.h
	gcc -o plate PlatePA3.o hash.o list.o

PlatePA3.o: PlatePA3.c hash.c list.c Hash.h
	gcc -c PlatePA3.c hash.c list.c

hash.o: hash.c list.c Hash.h
	gcc -c hash.c list.c

clean:
	rm plate *.o
