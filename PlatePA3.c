//By:David Bratkov
//This is the main function for the Hashtable assignment where it accepts a database and hashes it into a tabe of specified size (if not specied it will be a default of 100)
//GIT PUSH MORE OFTEN!!!
	
#include "Hash.h"

int hsize;
	
hashEntry *hashInit(int); //These are all the prototypes of the functions that will be called in the main file
void hashAdd(hashEntry*, char*, char*, char*);
int hashFind(hashEntry*, char*, char*, char*);
void hashDump(hashEntry*, int);
void hashFree(hashEntry*);	
void hashLoad(hashEntry*);

int main(int argc, char *argv[]){
	
	int temp=0, flag;
	FILE *file;
	
	if (argc == 2){//This stores the database into a file
	hsize=100;
	file=fopen(argv[1], "r");
	temp=1;
	}
	
	if (argc == 3){//This gets the size for the hashtable from the user
	file=fopen(argv[1], "r");
	temp=sscanf(argv[2],"%i",&hsize);
	}
	
	if (argc =! 2 && argc != 3 || file == NULL || temp != 1) {
	printf("Please run the command with a database: plate [file] [hash table size]\n");
	return(1);
	}
	
	//printf("DEBUG hashsize=%i temp=%i\n",hsize, temp);
	
	char buffer[120];
	flag=0;
	
	hashEntry *hashTable=hashInit(hsize);//This initilizes the hashtable
	
	char plate[32], first[32], last[32];
	int error=0;
	while (fgets(buffer,120,file) != NULL){ //This adds everything in the database given into the hashtable
	error=sscanf(buffer,"%s %s %s",plate,first,last);
	
		if (error == 3){
		//printf("DEBUG plate:%s first:%s last:%s\n",plate,first,last);
		hashAdd(hashTable,plate,first,last);
		}	
	}
	
	//fclose(file); //this closes the file to free up space
	flag=0;	
	int number=0;
	char command[32];
	printf("Enter command or plate: ");
	while (NULL != fgets(buffer,32,stdin)){ //Loop waiting until user enters EOF
	int flag2=0;
		flag=sscanf(buffer,"%s %i",command,&number);
		//printf("DEBUG flag:%i command:%s number:%i\n",flag,command,number);
		if (strcmp(command,"*DUMP") == 0){ //The DUMP command
			if (number > hsize-1) flag2=-1;//check if user is checking outside of the table size
			if (number < 0) flag2=-2;
			if (flag2 >= 0){
				if (flag == 2) hashDump(hashTable,number);
				if (flag == 1){//Checks to see if there was an additional statement for the value of the cell to DUMP
					for (int i=0;i<hsize;i++){
						hashDump(hashTable,i);
					}
				}
			}
			if (flag2 < 0) printf("Error: Please input a number between 0 and %i\n",hsize-1); 
		flag2=1;
		}
		
		if (strcmp(command,"*LOAD") == 0){ //The LOAD Command
			hashLoad(hashTable);
		flag2=2;
		}
		if (flag2 == 0){ //this will search for the plate and return first and last 
		error=hashFind(hashTable,command,first,last);
			if (error == 0) printf("Plate not found!\n");
			if (error == 1) printf("First name:%s\nLast name:%s\n",first,last);
		}
	printf("Enter command or plate: ");
	}
printf("\nFreeing memory!\n");
fclose(file);//Frees up memory for valgrind
hashFree(hashTable);
}

