//By: David Bratkov
//This is the .h file that would be included in every function
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct node{
 char *plate;
 char *first;
 char *last;
 struct node *next;
};

typedef struct node* hashEntry;

extern int hsize; //global variable
