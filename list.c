//By:David Bratkov
//This is a function where it will insert a given entry to the beginning of the list
#include "Hash.h"
	
void listAdd(struct node *sent, char *p, char *f, char *l){
	
	/*strcpy(plate,p);
	strcpy(first,f); //This segfaults
	strcpy(last,l);*/
	
	//printf("DEBUGLISTADD plate:%s first:%s last:%s\n",p,f,l);
	struct node *temp=malloc(sizeof(struct node));
	
	temp -> plate = malloc(strlen(p)+1);
	temp -> first = malloc(strlen(f)+1);
	temp -> last = malloc(strlen(l)+1);
	
	strcpy(temp -> plate,p);
	strcpy(temp -> first,f);
	strcpy(temp -> last,l);
	temp -> next = sent -> next;
	sent -> next = temp;
}
//By:David Bratkov
//This is a function that searches for the plate in the linked list
//if it finds it, it updates the first and last chars to match the plate entry

int listFind(struct node *sent, char *plate, char *first, char *last){
	struct node *temp;
	temp=sent -> next;
	//printf("DEBUGLISTFIND plate:%s\n",plate);
	while (0 != strcmp(temp -> plate, plate) && temp -> next != NULL){
		temp=temp -> next;
	}
	if (strcmp(temp -> plate,plate) == 0){
	//printf("DEBUGLISTFIND before first:%s last:%s\n",first,last);
		strcpy(first,temp -> first);
		strcpy(last,temp -> last);
	//printf("DEBUGLISTFIND after first:%s last:%s\n",first,last);
		return(1);
	}
	return(0);
}
//By:David Bratkov
//This function essentially deletes the entire linked list and freeing up the memeory

int listFree(struct node *LL){
	
	struct node *prev;
	
	if (LL -> next == NULL) return(0);
	
	while (LL -> next != NULL){
		prev = LL;
		LL = LL -> next;
		//There is additional statements to see if its trying to free a sent node
		if (prev -> plate != "SENT") free(prev -> plate);
		if (prev -> first != "SENT") free(prev -> first);
		if (prev -> last != "SENT") free(prev -> last);
		free(prev);
	}
	if(LL -> next == NULL){
		free(LL -> plate);
		free(LL -> first);
		free(LL -> last);
		free(LL);
		return(1);
	}
	return(0);
}
//By:David Bratkov
//This function creates a sentinel node and returns it

struct node *listInit(){

	struct node *sentinel = malloc(sizeof(struct node));
	sentinel -> plate = "SENT";
	sentinel -> first = "SENT";
	sentinel -> last = "SENT";
	sentinel -> next = NULL;	
	return(sentinel);
}
//By:David Bratkov
//This is a function that will count the amount of nodes and return a int


int listLen(struct node *sent){
	int count=0;
	while (sent -> next != NULL){
		sent = sent -> next;
		count++;
	}
	if (sent -> next == NULL) return(count);
return(-1);
}
//By:David Bratkov
//This program prints the node given with the certain requirments as stated in PA3

void listPrint(struct node* LL){
	while (LL -> next != NULL){
		LL=LL -> next;
		printf("Plate: <%s>  Name: %s, %s \n", LL -> plate, LL -> first, LL -> last);
	}
}
