//By:David Bratkov
//This function will add a entry into a already existing hash table
//this will not increase the array of the hash table it will only add a entry
#include "Hash.h"

void listAdd(struct node *,char *, char *,char *);

void hashAdd(hashEntry *hashTable, char *plate, char *first, char *last){

	int tablesize=hsize;// use of global variable
	
	int hash=0;

	for (int i=0;i<strlen(plate)-2;i++){
		hash+=(i+5)*plate[i];
	}
	hash=hash%tablesize;

	listAdd(hashTable[hash],plate,first,last);
	
}
//By:David Bratkov
//This function recives a pointer to the hashtable and a number of what cell and it prints out how long the linked list is and all the info in each cell

//int listLen(struct node *);
void listPrint(struct node *);

void hashDump(hashEntry *hashTable, int cellNum){
	
	//int count=0;
	
	//count=listLen(hashTable[cellNum]);
	
	printf("Contents of cell %i\n",cellNum);
		
	listPrint(hashTable[cellNum]);
	
	printf("----------------------------------\n");
}
//By:David Bratkov
//This is a function that will search for a plate number and return 1 if found and 0 if not found


int listFind(struct node *, char *, char *, char *);

int hashFind(hashEntry *hashTable, char *plate, char *first, char *last){
	for (int i=0;i < hsize;i++){
		if (listFind(hashTable[i], plate, first, last) == 1) return(1);
	}
return(0);
}
//By:David Bratkov
//This function will free up all the cells in the hash table by calling each one with the listFree function
void listFree(struct node *);
void hashFree(hashEntry *hashTable){
	for (int i=0;i < hsize;i++){
		listFree(hashTable[i]);
	}
free(hashTable);
}
//By: David Bratkov
//This program recives a int and makes a hash table of that size
struct node *listInit();

hashEntry *hashInit(int hashsize){
	hashEntry *hash=malloc(sizeof(hashEntry([hashsize])));	
	for (int x=0;x < hashsize;x++){
		hash[x]=listInit();
	}	
return(hash);
}
//By: David Bratkov
//This Program will state what cell its in then will call the listLen which will return a value that will show us how much cells in that Linked List
int listLen(struct node *);
void hashLoad(hashEntry *hashTable){
	int retv=0;
	for (int i=0;i < hsize;i++){
		printf("Entry %i, length=%i\n",i,retv=listLen(hashTable[i]));
	} 
}
